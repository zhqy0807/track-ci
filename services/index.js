const express = require('express');
const cors = require('cors');

const app = express();

// Access-Control-Allow-Origin.
app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', 'http://zhuangzhong.bugfree.im');
  res.set('Access-Control-Allow-Credentials', true);
  return next();
});

// use cors.
// app.use(cors());

// /
app.get('/', (req, res) => {
    return res.json({message: 'hello track ci'});
});

app.listen(3000, () => {
    console.log('server is running on port 3000');
})
