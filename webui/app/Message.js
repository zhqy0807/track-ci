import React, { Component } from 'react';
import fetch from 'node-fetch';

class Message extends Component {
  constructor() {
    super();
    this.state = {
      text: 'Waiting...'
    }
  }

  componentDidMount(){
    let self = this;
    fetch('http://track.ci.pixelcat.cc:80')
      .then((res) => {
        // console.log('get response from server');
        return res.json();
      })
      .then((data) => {
        if(data.message){
          self.setState({text: data.message});
        }else{
          self.setState({text: 'No response'});
        }
      })
  }

  render(){
    return (
      <h5 style={{color: "blue"}}>
        {this.state.text}
      </h5>
    )
  }
}

module.exports = Message;
