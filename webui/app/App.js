import React, { Component } from 'react';
import {render} from 'react-dom';

import Message from './Message';

class App extends Component {
  render(){
    return (
      <Message />
    );
  }
}

render(<App />, document.getElementById('root'));
